trello2wr
=========

Generates weekly work report (A&amp;O) from Trello board


#### config .yml  (~/.trello2wr/config.yml )
trello:
  developer_public_key: https://trello.com/1/appKey/generate (Developer API Keys)  
  member_token: https://trello.com/1/connect?key=DEVELOPER_PUBLIC_KEY&name=trello2wr.rb&response_type=token  
  username: see Trello Profile
  
email:
  client: (thunderbird |  kmail | evolition)  
  sender: email  
  recipient: email  

